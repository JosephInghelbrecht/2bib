<?php
include('../config/utripadmin.php');
include('../vendor/threepenny/CRUD.php');
$json = file_get_contents('neolithicum.json');
$data = json_decode($json, TRUE);
echo '<pre>';
//var_dump($data);
echo '</pre>';
foreach($data['curiosity'] as $item) {
    $row = array(
        "Name" => $item['name'],
        "Image" => $item['image'],
        "Description" => $item['comment'],
        "Type" => $item['type'],
        "Period" => $item['period'],
        "Coordinates" => $item['coordinates'],
        "Longitude" => $item['longitude'],
        "Latitude" => $item['latitude'],
        "Country" => $item['country'],
        "Region" => $item['region'],
        "ProvDep" => $item['departement'],
        "City" => $item['city'],
        "Likes" => 0,
        "InsertedOn" => date('Y-m-d H:i:s'));
    if (\Threepenny\CRUD::create('Curiosity', $row, 'Name')) {
        echo "Rij toegevoegd! {$row['Name']} is toegevoegd aan Curiosity";
    } else {
        echo "Oeps er is iets fout gelopen! Kan {$row['Name']} niet toevoegen aan Curiosity";
        echo \Threepenny\CRUD::getMessage();
    }
}
