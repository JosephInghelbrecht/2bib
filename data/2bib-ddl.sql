-- 3penny -- Tuesday 4th of May 2021 02:41:27 PM
--
SET
GLOBAL TRANSACTION ISOLATION LEVEL SERIALIZABLE;
-- mode changes syntax and behavior to conform more closely to standard SQL.
-- It is one of the special combination modes listed at the end of this section.
SET
GLOBAL sql_mode = 'ANSI';
-- If database does not exist, create the database
CREATE
DATABASE IF NOT EXISTS Biblio2;
USE
`Biblio2`;
-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET
FOREIGN_KEY_CHECKS = 0;

-- modernways.be
-- created by 3penny
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE User
-- Created on Tuesday 4th of May 2021 02:41:27 PM
--
DROP TABLE IF EXISTS `User`;
CREATE TABLE `User`
(
    `Name`       NVARCHAR (50) NOT NULL,
    `Email`      NVARCHAR (255) NOT NULL,
    `Password`   NVARCHAR (255) NULL,
    `FirstName`  NVARCHAR (50) NOT NULL,
    `LastName`   NVARCHAR (100) NOT NULL,
    `Phone`      VARCHAR(25) NOT NULL,
    `Street`     NVARCHAR (255) NULL,
    `PostalCode` VARCHAR(20) NULL,
    `City`       NVARCHAR (80) NULL,
    `Role`       NVARCHAR (20) NOT NULL,
    `Id`         INT         NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY (`Id`),
    `UpdatedOn`  TIMESTAMP   NOT NULL,
    `LastLogin`  TIMESTAMP   NOT NULL,
    CONSTRAINT uc_User_Name UNIQUE (Name)
);

-- modernways.be
-- created by 3penny
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Book
-- Created on Tuesday 4th of May 2021 02:41:27 PM
--
DROP TABLE IF EXISTS `Book`;
CREATE TABLE `Book`
(
    `Author`          NVARCHAR (255) NOT NULL,
    `Title`           NVARCHAR (255) NOT NULL,
    `Subtitle`        NVARCHAR (255) NULL,
    `ProductCode`     NVARCHAR (50) NULL,
    `ProductType`     NVARCHAR (50) NULL,
    `ProductTypeFull` NVARCHAR (80) NULL,
    `Publisher`       NVARCHAR (120) NULL,
    `PublicationDate` VARCHAR(20) NULL,
    `Category`        NVARCHAR (255) NULL,
    `NumberOfPages`   VARCHAR(6) NULL,
    `Language`        NVARCHAR (20) NULL,
    `Size`            NVARCHAR (20) NULL,
    `Price`           DECIMAL(6, 2) NULL,
    `Description`     VARCHAR(2000) NULL,
    `ImageURL`        VARCHAR(255) NULL,
    `ConditionId`     INT NULL,
    `UserId`          INT NULL,
    `UpdatedOn`       TIMESTAMP NOT NULL,
    `Id`              INT       NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY (`Id`),
    CONSTRAINT fk_BookConditionId FOREIGN KEY (`ConditionId`) REFERENCES `Condition` (`Id`),
    CONSTRAINT fk_BookUserId FOREIGN KEY (`UserId`) REFERENCES `User` (`Id`)
);

-- modernways.be
-- created by 3penny
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Condition
-- Created on Tuesday 4th of May 2021 02:41:27 PM
--
DROP TABLE IF EXISTS `Condition`;
CREATE TABLE `Condition`
(
    `Name`        NVARCHAR (50) NOT NULL,
    `Description` NVARCHAR (255) NULL,
    `Id`          INT       NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY (`Id`),
    `UpdatedOn`   TIMESTAMP NOT NULL,
    CONSTRAINT uc_Condition_Name UNIQUE (Name)
);

-- modernways.be
-- created by 3penny
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE OrderStatus
-- Created on Tuesday 4th of May 2021 02:41:27 PM
--
DROP TABLE IF EXISTS `OrderStatus`;
CREATE TABLE `OrderStatus`
(
    `Name`        NVARCHAR (255) NOT NULL,
    `Description` NVARCHAR (1024) NULL,
    `Id`          INT       NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY (`Id`),
    `UpdatedOn`   TIMESTAMP NOT NULL,
    CONSTRAINT uc_OrderStatus_Name UNIQUE (Name)
);

-- modernways.be
-- created by 3penny
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE ShippingMethod
-- Created on Tuesday 4th of May 2021 02:41:27 PM
--
DROP TABLE IF EXISTS `ShippingMethod`;
CREATE TABLE `ShippingMethod`
(
    `Name`        NVARCHAR (255) NOT NULL,
    `Description` NVARCHAR (1024) NULL,
    `Price`       DECIMAL(6, 2) NOT NULL,
    `Id`          INT           NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY (`Id`),
    `UpdatedOn`   TIMESTAMP     NOT NULL,
    CONSTRAINT uc_ShippingMethod_Name UNIQUE (Name)
);

-- modernways.be
-- created by 3penny
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Order
-- Created on Tuesday 4th of May 2021 02:41:27 PM
--
DROP TABLE IF EXISTS `Order`;
CREATE TABLE `Order`
(
    `OrderDate`        DATETIME  NOT NULL,
    `ShippingDate`     DATETIME  NOT NULL,
    `Comment`          NVARCHAR (512) NULL,
    `CustomerId`       INT       NOT NULL,
    `ShippingMethodId` INT       NOT NULL,
    `StatusId`         INT       NOT NULL,
    `UpdatedOn`        TIMESTAMP NOT NULL,
    `Id`               INT       NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY (`Id`),
    CONSTRAINT fk_OrderCustomerId FOREIGN KEY (`CustomerId`) REFERENCES `Customer` (`Id`),
    CONSTRAINT fk_OrderShippingMethodId FOREIGN KEY (`ShippingMethodId`) REFERENCES `ShippingMethod` (`Id`),
    CONSTRAINT fk_OrderStatusId FOREIGN KEY (`StatusId`) REFERENCES `OrderStatus` (`Id`)
);

-- modernways.be
-- created by 3penny
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE OrderItem
-- Created on Tuesday 4th of May 2021 02:41:27 PM
--
DROP TABLE IF EXISTS `OrderItem`;
CREATE TABLE `OrderItem`
(
    `BookId`    INT       NOT NULL,
    `OrderId`   INT       NOT NULL,
    `Id`        INT       NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY (`Id`),
    `Quantity`  DECIMAL(4, 2) NULL,
    `UpdatedOn` TIMESTAMP NOT NULL,
    CONSTRAINT fk_OrderItemBookId FOREIGN KEY (`BookId`) REFERENCES `Product` (`Id`),
    CONSTRAINT fk_OrderItemOrderId FOREIGN KEY (`OrderId`) REFERENCES `Order` (`Id`)
);

-- modernways.be
-- created by 3penny
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Customer
-- Created on Tuesday 4th of May 2021 02:41:27 PM
--
DROP TABLE IF EXISTS `Customer`;
CREATE TABLE `Customer`
(
    `Email`      NVARCHAR (255) NOT NULL,
    `FirstName`  NVARCHAR (255) NOT NULL,
    `LastName`   NVARCHAR (255) NOT NULL,
    `Address`    NVARCHAR (255) NOT NULL,
    `City`       NVARCHAR (255) NOT NULL,
    `PostalCode` VARCHAR(20) NOT NULL,
    `Country`    NVARCHAR (50) NULL,
    `Phone`      VARCHAR(40) NULL,
    `Id`         INT         NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY (`Id`),
    `UpdatedOn`  TIMESTAMP   NOT NULL,
    CONSTRAINT uc_Customer_Email UNIQUE (Email)
);

-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET
FOREIGN_KEY_CHECKS = 1;

