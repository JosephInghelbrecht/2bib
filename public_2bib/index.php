<?php
// You should create the include directory at the username directory level (that is, one level above the public_html directory).
// This ensures that sensitive files are not in the public_html directory, which anyone can access.
include('../config/2bib.php');

include('../vendor/threepenny/mvc/FrontController.php');
include('../vendor/threepenny/mvc/Controller.php');
include('../vendor/threepenny/CRUD.php');

include('Controllers/AdminController.php');
include('Controllers/BookController.php');
include('Controllers/ConditionController.php');
include('Controllers/UserController.php');

// default namespace is root \, otherwise specify it as argument
// default controller name is , otherwise specify it as argument
// default action method name is , otherwise specify it as argument
$route = \Threepenny\MVC\FrontController::getRouteData($_SERVER['REQUEST_URI'], '_2bib', 'Admin', 'index');
$view = \Threepenny\MVC\FrontController::Dispatch($route);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>2bib</title>
</head>
<body>
<header class="front">
    <nav class="control-panel">
        <a class="tile" href="/Admin/index">
            <span class="icon-menu2"></span><span class="screen-reader-text">Beheer index</span></a>
    </nav>
    <h1>2bib</h1>
</header>
<!-- main element not here, has different class for different purposes -->
<?php $view(); ?>
<footer>
    <p>&copy ModernWays 2021</p>
    <p>Projectwerk 2bib</p>
</footer>
</body>
</html>