<!--  ReadingOne View for Condition entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 4th of May 2021 03:28:25 PM
 file name Views/Condition/ReadingOne.php/ReadingOne.php
-->
<main class="show-room entity">
	<section class="detail" id="form" action="/Condition/createOne" method="post">
		<header>
			<h2 class="banner">Reading One Condition</h2>
			<nav class="command-panel">
				<a href="/Condition/UpdatingOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-pencil"></span>
					<span class="screen-reader-text">Updating One</span>
				</a>
				<a href="/Condition/CreatingOne" class="tile">
					<span class="icon-plus"></span>
					<span class="screen-reader-text">Creating One</span>
				</a>
				<a href="/Condition/DeleteOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-bin"></span>
					<span class="screen-reader-text">Delete One</span>
				</a>
				<a href="/Condition/Index" class="tile">
					<span class="icon-cross"></span>
					<span class="screen-reader-text">Annuleren</span>
				</a>
			</nav>
		</header>
		<fieldset>
			<div class="field">
				<label for="Condition-Name">Naam</label>
				<input id="Condition-Name" name="Condition-Name" class="text" style="width: 12.5%;" type="text" value="<?php echo $model['row']['Name'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Condition-Description">Beschrijving</label>
				<input id="Condition-Description" name="Condition-Description" style="width: 80%;" type="text" value=""  disabled />
			</div>
			<div class="field">
				<input id="Condition-Id" name="Condition-Id" style="width: 6em;" type="hidden" value="<?php echo $model['row']['Id'];?>"   />
			</div>
			<div class="field">
				<label for="Condition-UpdatedOnDate">Laatst gewijzigd op</label>
				<input id="Condition-UpdatedOnDate" value="<?php echo date('Y-m-d', strtotime($model['row']['UpdatedOn']));?>" type="date" name="Condition-UpdatedOnDate"    />
				<label for="Condition-UpdatedOnTime">om</label>
				<input id="Condition-UpdatedOnTime"  value="<?php echo date('H:i:s', strtotime($model['row']['UpdatedOn']));?>" type="time" name="Condition-UpdatedOnTime"    />
			</div>
		</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</section>
	<?php include('ReadingAll.php'); ?>
</main>
