<!--  CreatingOne View for Condition entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 4th of May 2021 03:28:25 PM
 file name Views/Condition/CreatingOne.php/CreatingOne.php
-->
<main class="show-room entity">
	<form class="detail" id="form" action="/Condition/createOne" method="post">
		<header>
			<h2 class="banner">Creating One Condition</h2>
			<nav class="command-panel">
				<button type="submit" value="createOne" name="createOne" class="tile">
					<span class="icon-floppy-disk"></span>
					<span class="screen-reader-text">Create One</span>
				</button>
				<a href="/Condition/Index" class="tile">
					<span class="icon-cross"></span>
					<span class="screen-reader-text">Annuleren</span>
				</a>
			</nav>
		</header>
		<fieldset>
			<div class="field">
				<label for="Condition-Name">Naam</label>
				<input id="Condition-Name" name="Condition-Name" class="text" style="width: 12.5%;" type="text" value="" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="Condition-Description">Beschrijving</label>
				<input id="Condition-Description" name="Condition-Description" style="width: 80%;" type="text" value=""   />
			</div>
			<div class="field">
				<label for="Condition-UpdatedOnDate">Laatst gewijzigd op</label>
				<input id="Condition-UpdatedOnDate" value="<?php echo date('Y-m-d');?>" type="date" name="Condition-UpdatedOnDate"   required />
				<label for="Condition-UpdatedOnTime">om</label>
				<input id="Condition-UpdatedOnTime"  value="<?php echo date('H:i:s');?>" type="time" name="Condition-UpdatedOnTime"   required />
					<span>*</span>
			</div>
		</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</form>
	<?php include('ReadingAll.php'); ?>
</main>
