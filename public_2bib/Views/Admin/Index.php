<!--  Index view for Admin entities
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 4th of May 2021 03:28:25 PM
 file name Admin/Index.php
-->
<main class="show-room index">
<a href="/User/index" class="tile"><span>User</span></a>
<a href="/Book/index" class="tile"><span>Book</span></a>
<a href="/Condition/index" class="tile"><span>Condition</span></a>
<a href="/OrderStatus/index" class="tile"><span>OrderStatus</span></a>
<a href="/ShippingMethod/index" class="tile"><span>ShippingMethod</span></a>
<a href="/Order/index" class="tile"><span>Order</span></a>
<a href="/OrderItem/index" class="tile"><span>OrderItem</span></a>
<a href="/Customer/index" class="tile"><span>Customer</span></a>
</main>
