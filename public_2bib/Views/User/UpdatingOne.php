<!--  UpdatingOne View for User entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 4th of May 2021 03:28:25 PM
 file name Views/User/UpdatingOne.php/UpdatingOne.php
-->
<main class="show-room entity">
	<form class="detail" id="form" action="/User/UpdateOne" method="post">
		<header>
			<h2 class="banner">Updating One User</h2>
			<nav class="command-panel">
				<button type="submit" value="updateOne" name="updateOne" class="tile">
					<span class="icon-floppy-disk"></span>
					<span class="screen-reader-text">Update One</span>
				</button>
				<a href="/User/Index" class="tile">
					<span class="icon-cross"></span>
					<span class="screen-reader-text">Annuleren</span>
				</a>
			</nav>
		</header>
		<fieldset>
			<div class="field">
				<label for="User-Name">Naam</label>
				<input id="User-Name" name="User-Name" class="text" style="width: 12.5%;" type="text" value="<?php echo $model['row']['Name'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="User-Email">Email</label>
				<input id="User-Email" name="User-Email" style="width: 80%;" type="email" value="<?php echo $model['row']['Email'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="User-Password">Hash</label>
				<input id="User-Password" name="User-Password" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['Password'];?>"   />
			</div>
			<div class="field">
				<label for="User-FirstName">Voornaam</label>
				<input id="User-FirstName" name="User-FirstName" class="text" style="width: 12.5%;" type="text" value="<?php echo $model['row']['FirstName'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="User-LastName">Familienaam</label>
				<input id="User-LastName" name="User-LastName" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['LastName'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="User-Phone">Tel</label>
				<input id="User-Phone" name="User-Phone" class="text" style="width: 6.25%;" type="text" value="<?php echo $model['row']['Phone'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="User-Street">Adres</label>
				<input id="User-Street" name="User-Street" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['Street'];?>"   />
			</div>
			<div class="field">
				<label for="User-PostalCode">Postcode</label>
				<input id="User-PostalCode" name="User-PostalCode" class="text" style="width: 5%;" type="text" value="<?php echo $model['row']['PostalCode'];?>"   />
			</div>
			<div class="field">
				<label for="User-City">Woonplaats</label>
				<input id="User-City" name="User-City" class="text" style="width: 20%;" type="text" value="<?php echo $model['row']['City'];?>"   />
			</div>
			<div class="field">
				<label for="User-Role">Rol</label>
				<select id="User-Role" name="User-Role" required >
					<option value="Admin" <?php echo ($model['row']['Role'] == 'Admin' ? ' selected' : '');?>>Admin</option>
					<option value="Vendor" <?php echo ($model['row']['Role'] == 'Vendor' ? ' selected' : '');?>>Vendor</option>
					<option value="Member" <?php echo ($model['row']['Role'] == 'Member' ? ' selected' : '');?>>Member</option>
					<option value="Guest" <?php echo ($model['row']['Role'] == 'Guest' ? ' selected' : '');?>>Guest</option>
				</select>
					<span>*</span>
			</div>
			<div class="field">
				<input id="User-Id" name="User-Id" style="width: 6em;" type="hidden" value="<?php echo $model['row']['Id'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="User-UpdatedOnDate">Laatst gewijzigd op</label>
				<input id="User-UpdatedOnDate" value="<?php echo date('Y-m-d', strtotime($model['row']['UpdatedOn']));?>" type="date" name="User-UpdatedOnDate"   required />
				<label for="User-UpdatedOnTime">om</label>
				<input id="User-UpdatedOnTime"  value="<?php echo date('H:i:s', strtotime($model['row']['UpdatedOn']));?>" type="time" name="User-UpdatedOnTime"   required />
					<span>*</span>
			</div>
			<div class="field">
				<label for="User-LastLoginDate">Laatst aangemeld op</label>
				<input id="User-LastLoginDate" value="<?php echo date('Y-m-d', strtotime($model['row']['LastLogin']));?>" type="date" name="User-LastLoginDate"   required />
				<label for="User-LastLoginTime">om</label>
				<input id="User-LastLoginTime"  value="<?php echo date('H:i:s', strtotime($model['row']['LastLogin']));?>" type="time" name="User-LastLoginTime"   required />
					<span>*</span>
			</div>
		</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</form>
	<?php include('ReadingAll.php'); ?>
</main>
