<!--  CreatingOne View for User entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 4th of May 2021 03:28:25 PM
 file name Views/User/CreatingOne.php/CreatingOne.php
-->
<main class="show-room entity">
	<form class="detail" id="form" action="/User/createOne" method="post">
		<header>
			<h2 class="banner">Creating One User</h2>
			<nav class="command-panel">
				<button type="submit" value="createOne" name="createOne" class="tile">
					<span class="icon-floppy-disk"></span>
					<span class="screen-reader-text">Create One</span>
				</button>
				<a href="/User/Index" class="tile">
					<span class="icon-cross"></span>
					<span class="screen-reader-text">Annuleren</span>
				</a>
			</nav>
		</header>
		<fieldset>
			<div class="field">
				<label for="User-Name">Naam</label>
				<input id="User-Name" name="User-Name" class="text" style="width: 12.5%;" type="text" value="" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="User-Email">Email</label>
				<input id="User-Email" name="User-Email" style="width: 80%;" type="email" value="" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="User-Password">Hash</label>
				<input id="User-Password" name="User-Password" class="text" style="width: 80%;" type="text" value=""   />
			</div>
			<div class="field">
				<label for="User-FirstName">Voornaam</label>
				<input id="User-FirstName" name="User-FirstName" class="text" style="width: 12.5%;" type="text" value="" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="User-LastName">Familienaam</label>
				<input id="User-LastName" name="User-LastName" class="text" style="width: 80%;" type="text" value="" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="User-Phone">Tel</label>
				<input id="User-Phone" name="User-Phone" class="text" style="width: 6.25%;" type="text" value="" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="User-Street">Adres</label>
				<input id="User-Street" name="User-Street" class="text" style="width: 80%;" type="text" value=""   />
			</div>
			<div class="field">
				<label for="User-PostalCode">Postcode</label>
				<input id="User-PostalCode" name="User-PostalCode" class="text" style="width: 5%;" type="text" value=""   />
			</div>
			<div class="field">
				<label for="User-City">Woonplaats</label>
				<input id="User-City" name="User-City" class="text" style="width: 20%;" type="text" value=""   />
			</div>
			<div class="field">
				<label for="User-Role">Rol</label>
				<select id="User-Role" name="User-Role" required >
					<option value="Admin" >Admin</option>
					<option value="Vendor" >Vendor</option>
					<option value="Member" >Member</option>
					<option value="Guest" >Guest</option>
				</select>
					<span>*</span>
			</div>
			<div class="field">
				<label for="User-UpdatedOnDate">Laatst gewijzigd op</label>
				<input id="User-UpdatedOnDate" value="<?php echo date('Y-m-d');?>" type="date" name="User-UpdatedOnDate"   required />
				<label for="User-UpdatedOnTime">om</label>
				<input id="User-UpdatedOnTime"  value="<?php echo date('H:i:s');?>" type="time" name="User-UpdatedOnTime"   required />
					<span>*</span>
			</div>
			<div class="field">
				<label for="User-LastLoginDate">Laatst aangemeld op</label>
				<input id="User-LastLoginDate" value="<?php echo date('Y-m-d');?>" type="date" name="User-LastLoginDate"   required />
				<label for="User-LastLoginTime">om</label>
				<input id="User-LastLoginTime"  value="<?php echo date('H:i:s');?>" type="time" name="User-LastLoginTime"   required />
					<span>*</span>
			</div>
		</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</form>
	<?php include('ReadingAll.php'); ?>
</main>
