<!--  ReadingAll view for Book entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 4th of May 2021 03:28:25 PM
 file name Views/Book/ReadingAll.php/ReadingAll.php
-->
<aside class="list">
	<?php
		if ($model['list'])
		{
	?>
	<table>
		<?php
			foreach ($model['list'] as $item)
			{
		?>
		<tr>
			<td>
				<a class="tile"
				href="/Book/readingOne/<?php echo $item['Id'];?>">
				<span class="icon-arrow-right"></span>
				<span class="screen-reader-text">Select</span></a>
			</td>
			<td>
				<?php echo $item['Author'];?>
			</td>
			<td>
				<?php echo $item['Title'];?>
			</td>
			<td>
				<?php echo $item['ConditionIdName'];?>
			</td>
			<td>
				<?php echo $item['UserIdName'];?>
			</td>
			<td>
				<?php echo $item['Id'];?>
			</td>

		</tr>
		<?php
		}
		?>
	</table>
	<?php
		}
		else
		{
	?>
	<p>Geen Book</p>
	<?php
	}
	?>
</aside>

