<!--  ReadingOne View for Book entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 4th of May 2021 03:28:25 PM
 file name Views/Book/ReadingOne.php/ReadingOne.php
-->
<main class="show-room entity">
	<section class="detail" id="form" action="/Book/createOne" method="post">
		<header>
			<h2 class="banner">Reading One Book</h2>
			<nav class="command-panel">
				<a href="/Book/UpdatingOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-pencil"></span>
					<span class="screen-reader-text">Updating One</span>
				</a>
				<a href="/Book/CreatingOne" class="tile">
					<span class="icon-plus"></span>
					<span class="screen-reader-text">Creating One</span>
				</a>
				<a href="/Book/DeleteOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-bin"></span>
					<span class="screen-reader-text">Delete One</span>
				</a>
				<a href="/Book/Index" class="tile">
					<span class="icon-cross"></span>
					<span class="screen-reader-text">Annuleren</span>
				</a>
			</nav>
		</header>
		<fieldset>
			<div class="field">
				<label for="Book-Author">Auteur</label>
				<input id="Book-Author" name="Book-Author" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['Author'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Book-Title">Titel</label>
				<input id="Book-Title" name="Book-Title" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['Title'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Book-Subtitle">Ondertitel</label>
				<input id="Book-Subtitle" name="Book-Subtitle" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['Subtitle'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Book-ProductCode">Productcode</label>
				<input id="Book-ProductCode" name="Book-ProductCode" style="width: 12.5%;" type="text" value=""  disabled />
			</div>
			<div class="field">
				<label for="Book-ProductType">Producttype</label>
				<input id="Book-ProductType" name="Book-ProductType" style="width: 12.5%;" type="text" value=""  disabled />
			</div>
			<div class="field">
				<label for="Book-ProductTypeFull">Productcode</label>
				<input id="Book-ProductTypeFull" name="Book-ProductTypeFull" style="width: 20%;" type="text" value=""  disabled />
			</div>
			<div class="field">
				<label for="Book-Publisher">Uitgever</label>
				<input id="Book-Publisher" name="Book-Publisher" style="width: 80%;" type="text" value=""  disabled />
			</div>
			<div class="field">
				<label for="Book-PublicationDate">Verschijningsjaar</label>
				<input id="Book-PublicationDate" name="Book-PublicationDate" style="width: 5%;" type="text" value=""  disabled />
			</div>
			<div class="field">
				<label for="Book-Category">Categorie</label>
				<input id="Book-Category" name="Book-Category" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['Category'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Book-NumberOfPages">Aantal paginas</label>
				<input id="Book-NumberOfPages" name="Book-NumberOfPages" class="text" style="width: 1.5%;" type="text" value="<?php echo $model['row']['NumberOfPages'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Book-Language">Taal</label>
				<input id="Book-Language" name="Book-Language" class="text" style="width: 5%;" type="text" value="<?php echo $model['row']['Language'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Book-Size">Afmetingen</label>
				<input id="Book-Size" name="Book-Size" class="text" style="width: 5%;" type="text" value="<?php echo $model['row']['Size'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Book-Price">Kostprijs</label>
				<input id="Book-Price" name="Book-Price" class="decimal" type="text" value="<?php echo $model['row']['Price'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Book-Description">Beschrijving</label>
				<input id="Book-Description" name="Book-Description" style="width: 80%;" type="texterea" value=""  disabled />
			</div>
			<div class="field">
				<label for="Book-ImageURL">Afbeelding</label>
				<input id="Book-ImageURL" name="Book-ImageURL" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['ImageURL'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Book-ConditionId">Conditie</label>
				<select id="Book-ConditionId" name="Book-ConditionId"  disabled>
				<?php
				if (count($model['ConditionList']) > 0)
				{
					$i = 1;
					foreach ($model['ConditionList'] as $item)
					{
					?>
					<option value="<?php echo $item['Id'];?>" <?php echo ($model['row']['ConditionId']  == $fkRow[Id] ? ' selected' : '');?>>
						<?php echo $item['Name'];?></option>
					<?php
					}
				}
				?>
				</select>
			</div>
			<div class="field">
				<label for="Book-UserId">Gebruiker</label>
				<select id="Book-UserId" name="Book-UserId"  disabled>
				<?php
				if (count($model['UserList']) > 0)
				{
					$i = 1;
					foreach ($model['UserList'] as $item)
					{
					?>
					<option value="<?php echo $item['Id'];?>" <?php echo ($model['row']['UserId']  == $fkRow[Id] ? ' selected' : '');?>>
						<?php echo $item['Name'];?></option>
					<?php
					}
				}
				?>
				</select>
			</div>
			<div class="field">
				<label for="Book-UpdatedOnDate">Laatst gewijzigd op</label>
				<input id="Book-UpdatedOnDate" value="<?php echo date('Y-m-d', strtotime($model['row']['UpdatedOn']));?>" type="date" name="Book-UpdatedOnDate"  disabled  />
				<label for="Book-UpdatedOnTime">om</label>
				<input id="Book-UpdatedOnTime"  value="<?php echo date('H:i:s', strtotime($model['row']['UpdatedOn']));?>" type="time" name="Book-UpdatedOnTime"  disabled  />
			</div>
			<div class="field">
				<input id="Book-Id" name="Book-Id" style="width: 6em;" type="hidden" value="<?php echo $model['row']['Id'];?>"   />
			</div>
		</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</section>
	<?php include('ReadingAll.php'); ?>
</main>
