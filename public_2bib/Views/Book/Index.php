<!--  Index view for Book entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 4th of May 2021 03:28:25 PM
 file name Views/Book/Index.php/Index.php
-->
<main class="show-room entity">
	<section class="detail">
	<header>
		<h2 class="banner">Index Book</h2>
		<nav class="command-panel">
			<a href="/Book/CreatingOne" class="tile">
				<span class="icon-plus"></span>
				<span class="screen-reader-text">Creating One</span>
			</a>
		</nav>
	</header>
	<fieldset>
	</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</section>
	<?php include('ReadingAll.php'); ?>
</main>
