<?php
//if (file_exists(__DIR__ . '/' . $_SERVER['REQUEST_URI'])) {
//    return false; // serve the requested resource as-is.
//} else {
//    include_once 'index.php';
//}

function _requestedFileExists ($requestUri) {
    $queryStart = strpos($requestUri, '?');
    if ($queryStart !== -1) {
        $requestUri = substr($requestUri, 0, $queryStart);
    }
    return file_exists(__DIR__ . DIRECTORY_SEPARATOR . $requestUri);
}


if (_requestedFileExists($_SERVER['REQUEST_URI'])) {
    return false;
}

require 'index.php';
